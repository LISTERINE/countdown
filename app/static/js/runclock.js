var clock;

var target_time = '08/19/2017'; // MM/DD/YYYY

$(document).ready(function() {
  var date  = moment(target_time, 'MM/DD/YYYY', true).toDate();
  var now   = new Date();
  var diff  = date.getTime()/1000 - now.getTime()/1000;

  clock = new FlipClock($('.clock'), diff, {
    clockFace: 'DailyCounter',
    countdown: true,
  });
});

