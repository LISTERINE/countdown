FROM python:latest
MAINTAINER Jonathan Ferretti

RUN apt-get update -y && apt-get install -y \
    python-dev \
    curl \
    wget

RUN mkdir -p /opt/timer/ && \
    mkdir -p /var/log/timer

WORKDIR /opt/timer

COPY requirements.txt /opt/timer/requirements.txt
RUN pip install -U pip
RUN pip install -r requirements.txt

COPY app /opt/timer/app
COPY run.py /opt/timer/run.py
COPY config.py /opt/timer/config.py
CMD python run.py
