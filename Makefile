dockerbuild:
	docker build --no-cache -t timer:latest .

dockerbuildcache:
	docker build -t timer:latest .

dockerrun:
	sudo rm -rf app/static/.webassets-cache
	make dockerbuildcache && docker-compose up -d

dockerclean:
	docker-compose kill && docker-compose rm

dockerexec:
	docker exec -it $$(docker-compose ps -q |head -n 1) /bin/bash
